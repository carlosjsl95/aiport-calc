package imd0412.parkinglot;

import static imd0412.parkinglot.ParkingLotType.LongTerm;
import static imd0412.parkinglot.ParkingLotType.ShortTerm;
import static imd0412.parkinglot.ParkingLotType.VIP;

import imd0412.parkinglot.calculator.Calculator;
public class Main
{
	public static void main(String[] args) {

		Calculator calculator = new Calculator();
		System.out.println("O gasto total com o Estacionamento a curto prazo foi de: R$" + calculator.calculateParkingCost(args[0],args[1],ShortTerm));
		System.out.println("O gasto total com o Estacionamento a longo prazo foi de: R$" + calculator.calculateParkingCost(args[0],args[1],LongTerm));
		System.out.println("O gasto total com o Estacionamento VIP foi de: R$" + calculator.calculateParkingCost(args[0],args[1],VIP));
	}
}
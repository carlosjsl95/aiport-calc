package imd0412.parkinglot.calculator;

import static imd0412.parkinglot.exception.InvalidDataType.NonexistentDate;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;

import imd0412.parkinglot.Constants;
import imd0412.parkinglot.ParkingLotType;
import imd0412.parkinglot.exception.InvalidDataException;

public class Calculator {

	public float calculateParkingCost(String checkin, String checkout, ParkingLotType type) {
		
		try {
			
			LocalDateTime checkinTime = LocalDateTime.parse(checkin, Constants.DATE_FORMATTER);
			LocalDateTime checkoutTime = LocalDateTime.parse(checkout, Constants.DATE_FORMATTER);

			Duration duration = Duration.between(checkinTime, checkoutTime);

			if (checkinTime.getYear() < 1970 || checkinTime.getYear() > 2018) {
				throw new InvalidDataException("A data de checkin está fora dos padroes estabelecidos",
						NonexistentDate);
			}

			if (checkoutTime.getYear() < 1970 || checkinTime.getYear() > 2019) {
				throw new InvalidDataException("A data de checkout está fora dos padroes estabelecidos",
						NonexistentDate);
			}

			if (duration.isNegative()) {
				throw new InvalidDataException("A data do checkout é menor que a do checkin", NonexistentDate);
			}

			if (type.name() == "VIP") {
				return calculateVIP(duration);
			}

			if (type.name() == "LongTerm") {
				return calculateELP(duration);
			}

			if (type.name() == "ShortTerm") {
				return calculateECP(duration);
			}

		} catch (DateTimeParseException exc) {
			
			System.err.printf("%s is not parsable!%n", exc);
			throw exc;
		} catch (InvalidDataException e) {
			e.printStackTrace();
		}

		return 0F;
	}

	float calculateVIP(Duration duration) {
		float TOTAL_COST = 500;

		Long numeroDeDias = duration.toDays();
		Long numeroDeHoras = duration.minusDays(numeroDeDias).toHours();
		Long numeroDeMinutos = duration.minusDays(numeroDeDias).minusHours(numeroDeHoras).toMinutes();

		if ((int) duration.toDays() == 7 && numeroDeMinutos != 0) {
			TOTAL_COST = TOTAL_COST + 100;
		}

		if ((int) duration.toDays() > 7 && (int) duration.toDays() < 14) {
			TOTAL_COST = TOTAL_COST + ((int) duration.toDays() - 7) * 100;
		}

		if ((int) duration.toDays() == 14) {
			TOTAL_COST = TOTAL_COST + ((int) duration.toDays() - 7) * 100;
			if (numeroDeMinutos != 0) {
				TOTAL_COST = TOTAL_COST + 80;
			}
		}

		if ((int) duration.toDays() > 14) {
			int costForFourteenDays = 700;
			TOTAL_COST = TOTAL_COST + costForFourteenDays + ((duration.toDays() - 14) * 80);
		}

		return TOTAL_COST;
	}

	float calculateECP(Duration duration) {
		float custoTotal = 0;

		Long numeroDeDias = duration.toDays();
		Long numeroDeHoras = duration.minusDays(numeroDeDias).toHours();
		Long numeroDeMinutos = duration.minusDays(numeroDeDias).minusHours(numeroDeHoras).toMinutes();

		if (numeroDeDias <= 1) {
			if(numeroDeDias == 1 && (numeroDeHoras > 0 || numeroDeMinutos > 0)) {
				custoTotal = 50 + numeroDeHoras * 2;
				if(numeroDeMinutos > 0)
					custoTotal = custoTotal + 2;
			}else {
				if (numeroDeHoras > 1 || (numeroDeHoras == 1 && numeroDeMinutos > 0)) {
					custoTotal = 8 + (numeroDeHoras - 1) * 2;// Adiciona o valor referente às horas que passaram além pa
					if (numeroDeMinutos > 0) {// Acrescenta o valores referente aos minutos que excederam e não completaram
						custoTotal = custoTotal + 2;
					}
				} else {
					custoTotal = 8;
				}
			}

		} else { // Trata o caso de mais de uma dia
			if (numeroDeDias <= 7) {
				if(numeroDeDias == 7 && numeroDeHoras > 0 && numeroDeMinutos > 0)
					custoTotal = numeroDeDias * 30;
				else
					custoTotal = numeroDeDias * 50;
			} else {
				custoTotal = numeroDeDias * 30;
			}

			if (numeroDeHoras > 0) {// Acrescenta o valore referente às horas passadas que não completaram um dia
				custoTotal = custoTotal + numeroDeHoras * 2;
			}
			if (numeroDeMinutos > 0) { // Acrescenta o valores referente aos minutos que excederam e não completaram
				custoTotal = custoTotal + 2;
			}			
			
		}

		return custoTotal;
	}

	float calculateELP(Duration duration) {
		float custoTotal = 0;

		Long numeroDeDias = duration.toDays();
		Long numeroDeHoras = duration.minusDays(numeroDeDias).toHours();
		Long numeroDeMinutos = duration.minusDays(numeroDeDias).minusHours(numeroDeHoras).toMinutes();

		if (numeroDeDias <= 1) {
			custoTotal = 70;
		} else { // Trata o caso de mais de uma dia
			if (numeroDeDias <= 7) {
				custoTotal = 70 + (numeroDeDias - 1) * 50;
				if (numeroDeHoras > 0 || numeroDeMinutos > 0) {
					custoTotal = custoTotal + 50;
				}
			} else {
				custoTotal = 70 + (numeroDeDias - 1) * 30;
			}

			if (numeroDeHoras > 0 || numeroDeMinutos > 0) {
				custoTotal = custoTotal + 50;
				if (numeroDeHoras > 0 || numeroDeMinutos > 0) {
					custoTotal = custoTotal + 30;
				}
			}

		}

		custoTotal = custoTotal + (numeroDeDias / 30) * 500;

		return custoTotal;
	}
}
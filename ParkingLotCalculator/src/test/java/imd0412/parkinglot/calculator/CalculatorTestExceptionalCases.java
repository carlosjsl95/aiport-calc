package imd0412.parkinglot.calculator;

import static imd0412.parkinglot.ParkingLotType.LongTerm;
import static imd0412.parkinglot.ParkingLotType.ShortTerm;
import static imd0412.parkinglot.ParkingLotType.VIP;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import imd0412.parkinglot.ParkingLotType;
import imd0412.parkinglot.exception.InvalidDataException;

@RunWith(Parameterized.class)
public class CalculatorTestExceptionalCases {
	Calculator calculator = new Calculator();

	@Parameters( name = "{3}" )
	public static Collection input() {
		return Arrays.asList(new Object [][] {
								{"2017.04.08 00:00","2017.04.07 23:59",ShortTerm,  "ECP | 0 <= tempo <= 1 hora | Limite  Min-: tempo = -1 minuto"},
								{"2017.04.08 00:00","2017.04.07 23:59",LongTerm,  "ELP | 0 <= tempo <= 1 dias | Min-: tempo = -1 minuto"},
								{"2017.04.08 00:00","2017.04.07 23:59",VIP, "EVP | 0 <= tempo <= 7 dias | Limite  Min-: tempo = -1 minuto"},
							});
	}
	@Parameter(0)
	public String checkin;

	@Parameter(1)
	public String checkout;

	@Parameter(2)
	public ParkingLotType type;
	
	@Parameter(3)
	public String description;
	
	@Test(expected = InvalidDataException.class)
	public void testCalculator() {
		calculator.calculateParkingCost(checkin, checkout, type);
	}
}

package imd0412.parkinglot.calculator;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
   CalculatorTest.class,
   CalculatorTestExceptionalCases.class
})

public class CalculatorTestSuiteCase {
	
}

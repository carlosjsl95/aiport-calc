package imd0412.parkinglot.calculator;

import static imd0412.parkinglot.ParkingLotType.VIP;
import static imd0412.parkinglot.ParkingLotType.ShortTerm;
import static imd0412.parkinglot.ParkingLotType.LongTerm;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import imd0412.parkinglot.ParkingLotType;

@RunWith(Parameterized.class)
public class CalculatorTest {
	Calculator calculator = new Calculator();
	
	@Parameters( name = "{4}" )
	public static Collection input() {
		return Arrays.asList(new Object [][] {
									{"2017.04.08 00:00","2017.04.08 00:00",ShortTerm,8.0F, "ECP | 0 <= tempo <= 1 hora | Limite  Min: tempo = 0 minuto"},
									{"2017.04.08 00:00","2017.04.08 00:01",ShortTerm,8.0F, "ECP | 0 <= tempo <= 1 hora | Limite  Min+: tempo = 1 minuto"},
									{"2017.04.08 00:00","2017.04.08 01:00",ShortTerm, 8.0F, "ECP | 0 <= tempo <= 1 hora | Limite  Max: tempo = 1 hora"},
									{"2017.04.08 00:00","2017.04.08 01:01",ShortTerm, 10.0F, "ECP | 0 <= tempo <= 1 hora | Limite  Max+: tempo = 1hora e 1 minuto"},
									{"2017.04.08 00:00","2017.04.08 00:59",ShortTerm, 8.0F, "ECP | 0 <= tempo <= 1 hora | Limite  Max-: tempo = 59 minutos"},
									
									{"2017.04.08 00:00","2017.04.08 01:01",ShortTerm, 10.0F, "ECP | 1 hora > tempo <= 24 horas | Limite  Min: tempo = 1 hora e 1 minuto"},
									{"2017.04.08 00:00","2017.04.08 01:02",ShortTerm, 10.0F, "ECP | 1 hora > tempo <= 24 horas | Limite  Min+: tempo = 1 hora e 2 minutos"},
									{"2017.04.08 00:00","2017.04.08 01:00",ShortTerm, 8.0F, "ECP | 1 hora > tempo <= 24 horas | Limite  Min-: tempo = 1 hora"},
									{"2017.04.08 00:00","2017.04.09 00:00",ShortTerm, 54.0F, "ECP | 1 hora > tempo <= 24 horas | Limite  Max: tempo = 24 horas"},
									{"2017.04.08 00:00","2017.04.09 00:01",ShortTerm, 52.0F, "ECP | 1 hora > tempo <= 24 horas | Limite  Max+: tempo = 24 horas e 1 minuto"},
									{"2017.04.08 00:00","2017.04.08 23:59",ShortTerm, 54.0F, "ECP | 1 hora > tempo <= 24 horas | Limite  Max-: tempo = 23 horas 59 minutos"},
									
									{"2017.04.08 00:00","2017.04.09 00:01",ShortTerm, 106.0F, "ECP | 1 dia > tempo <= 7 dia | Limite  Min: tempo = 1 dia e 1 minuto"},
									{"2017.04.08 00:00","2017.04.09 00:00",ShortTerm, 54.0F, "ECP | 1 dia > tempo <= 7 dia | Limite  Min+: tempo = 1 dia"},
									{"2017.04.08 00:00","2017.04.08 23:59",ShortTerm, 54.0F, "ECP | 1 dia > tempo <= 7 dia | Limite  Min-: tempo = 23 horas e 59 minutos"},
									{"2017.04.08 00:00","2017.04.15 00:00",ShortTerm, 692.0F, "ECP | 1 dia > tempo <= 7 dia | Limite  Max: tempo = 7 dias"},
									{"2017.04.08 00:00","2017.04.15 00:01",ShortTerm, 724.0F, "ECP | 1 dia > tempo <= 7 dia | Limite  Max+: tempo = 7 dias e 1 minuto"},
									{"2017.04.08 00:00","2017.04.14 23:59",ShortTerm, 692.0F, "ECP | 1 dia > tempo <= 7 dia | Limite  Max-: tempo = 6 dias, 23 horas 59 minutos"},
									
									{"2017.04.08 00:00","2017.04.15 00:00",ShortTerm, 692.0F, "ECP | tempo > 7 dia | Limite  Min: tempo = 7 dias"},
									{"2017.04.08 00:00","2017.04.15 00:01",ShortTerm, 724.0F, "ECP | tempo > 7 dia | Limite  Min+: tempo = 7 dias e 1 minuto"},
									{"2017.04.08 00:00","2017.04.14 23:59",ShortTerm, 692.0F, "ECP | tempo > 7 dia | Limite  Min-: tempo = 6 dias, 23 horas 59 minutos"},
									
									{"2017.04.08 00:00","2017.04.08 00:00",LongTerm, 70.0F, "ELP | 0 <= tempo <= 1 dias | Limite  Min: tempo = 0 minuto"},
									{"2017.04.08 00:00","2017.04.08 00:01",LongTerm, 70.0F, "ELP | 0 <= tempo <= 1 dias | Limite  Min+: tempo = 1 minuto"},
									{"2017.04.08 00:00","2017.04.09 00:00",LongTerm, 70.0F, "ELP | 0 <= tempo <= 1 dias | Max: tempo = 1 dia"},
									{"2017.04.08 00:00","2017.04.09 00:01",LongTerm, 120.0F, "ELP | 0 <= tempo <= 1 dias | Max+: tempo = 1 dia e 1 minuto"},
									{"2017.04.08 00:00","2017.04.08 23:59",LongTerm, 70.0F, "ELP | 0 <= tempo <= 1 dias | Max-: tempo = 23 horas e 59 minutos"},
									
									
									{"2017.04.08 00:00","2017.04.09 00:01",LongTerm, 120.0F, "ELP | 1 dia > tempo <= 7 dias | Min: tempo = 1 dia e 1 minuto"},
									{"2017.04.08 00:00","2017.04.09 00:02",LongTerm, 120.0F, "ELP | 1 dia > tempo <= 7 dias | Min+: tempo = 1 dia e 2 minutos"},
									{"2017.04.08 00:00","2017.04.09 00:00",LongTerm, 70.0F, "ELP | 1 dia > tempo <= 7 dias | Min-: tempo = 1 dia"},
									{"2017.04.08 00:00","2017.04.15 00:00",LongTerm, 370.0F, "ELP | 1 dia > tempo <= 7 dias | Max: tempo = 7 dias"},
									{"2017.04.08 00:00","2017.04.15 00:01",LongTerm, 400.0F, "ELP | 1 dia > tempo <= 7 dias | Max+: tempo = 7 dias e 1 minuto"},
									{"2017.04.08 00:00","2017.04.14 23:59",LongTerm, 370.0F, "ELP | 1 dia > tempo <= 7 dias | Max-: tempo = 6 dias, 23 horas 59 minutos"},
									
									{"2017.04.08 00:00","2017.04.15 00:01",LongTerm, 400.0F, "ELP | 7 dias < tempo < 30 dias | Min: tempo = 7 dias e 1 minuto"},
									{"2017.04.08 00:00","2017.04.15 00:02",LongTerm, 400.0F, "ELP | 7 dias < tempo < 30 dias | Min+: tempo = 7 dia e 2 minutos"},
									{"2017.04.08 00:00","2017.04.14 23:59",LongTerm, 370.0F, "ELP | 7 dias < tempo < 30 dias | Min-: tempo = 6 dias, 23 horas e 59 minutos"},
									{"2018.04.08 00:00","2018.05.07 23:59",LongTerm, 1060.0F, "ELP | 7 dias < tempo < 30 dias | Max: tempo = 29 dias, 23 horas e 59 minutos"},
									{"2018.04.08 00:00","2018.05.08 00:00",LongTerm, 1060.0F, "ELP | 7 dias < tempo < 30 dias | Max+: tempo = 30 dias"},
									{"2018.04.08 00:00","2018.05.08 23:58",LongTerm, 1060.0F, "ELP | 7 dias < tempo < 30 dias | Max-: tempo = 29 dias, 23 horas e 58 minutos"},
									
									{"2018.04.08 00:00","2018.05.08 00:00",LongTerm, 1060.0F, "ELP | tempo >= 30 dias | Min: tempo = 30 dias"},
									{"2018.04.08 00:00","2018.05.08 00:01",LongTerm, 1590.0F, "ELP | tempo >= 30 dias | Min+: tempo = 30 dias e 1 minuto"},
									{"2018.04.08 00:00","2018.05.07 23:59",LongTerm, 1590.0F, "ELP | tempo >= 30 dias | Min-: tempo = 29 dias, 23 horas e 59 minutos"},
									
									
									
									
									{"2017.04.08 00:00","2017.04.08 00:00",VIP,500.0F, "EVP | 0 <= tempo <= 7 dias | Limite  Min: tempo = 0 minuto"},
									{"2017.04.08 00:00","2017.04.08 00:01",VIP,500.0F, "EVP | 0 <= tempo <= 7 dias | Limite  Min+: tempo = 1 minuto"},
									{"2017.04.08 00:00","2017.04.15 00:00",VIP,500.0F, "EVP | 0 <= tempo <= 7 dias | Limite  Max: tempo = 7 dias"},
									{"2017.04.08 00:00","2017.04.15 00:01",VIP,600.0F, "EVP | 0 <= tempo <= 7 dias | Limite  Max+: tempo = 7 dias e 1 minuto"},
									{"2017.04.08 00:00","2017.04.14 23:59",VIP,500.0F, "EVP | 0 <= tempo <= 7 dias | Limite  Max-: tempo = 6 dias 23 horas e 59 minutos"},
									
									{"2017.04.08 00:00","2017.04.15 00:01",VIP,600.0F, "EVP | 7 dias > tempo <= 14 dias | Limite  Min: tempo = 7 dias e 1 minuto"},
									{"2017.04.08 00:00","2017.04.15 00:02",VIP,600.0F, "EVP | 7 dias > tempo <= 14 dias | Limite Min+: tempo = 7 dias e 2 minutos"},
									{"2017.04.08 00:00","2017.04.15 00:00",VIP,500.0F, "EVP | 7 dias > tempo <= 14 dias | Limite Min-: tempo = 7 dias"},
									{"2017.04.08 00:00","2017.04.22 00:00",VIP,1200.0F, "EVP | 7 dias > tempo <= 14 dias | Limite Max: tempo = 14 dias"},
									{"2017.04.08 00:00","2017.04.22 00:01",VIP,1280.0F, "EVP | 7 dias > tempo <= 14 dias | Limite Max+: tempo = 14 dias e 1 minuto"},
									{"2017.04.08 00:00","2017.04.21 23:59",VIP,1100.0F, "EVP | 7 dias > tempo <= 14 dias | Limite Max-: tempo = 13 dias, 23 horas e 59 minutos"},
									
									{"2017.04.08 00:00","2017.04.22 00:01",VIP,1280.0F, "EVP | tempo > 14 dias | Limite Min: tempo = 14 dias e 1 minuto"},
									{"2017.04.08 00:00","2017.04.22 00:02",VIP,1280.0F, "EVP | tempo > 14 dias | Limite Min+: tempo = 14 dias e 2 minutos"},
									{"2017.04.08 00:00","2017.04.22 00:00",VIP,1200.0F, "EVP | tempo > 14 dias | Limite Min-: tempo = 14 dias"},
									
									
							});
	}
	
	@Parameter(0)
	public String checkin;
	
	@Parameter(1)
	public String checkout;
	
	@Parameter(2)
	public ParkingLotType type;
	
	@Parameter(3)
	public float totalCost;
	
	@Parameter(4)
	public String description;
	
	@Test
	public void testCalculator() {	
		assertEquals(totalCost,calculator.calculateParkingCost(checkin, checkout, type),00);
	}
}
